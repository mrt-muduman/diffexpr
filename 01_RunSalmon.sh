#!/usr/bin/bash

# Traverse through given directory of paired-end seqeuencing data, and run
# Salmon to quantify expression at the transcript level.

# Author:   Mohamed Uduman (muduman@monterosatx.com)
# Date:     27 December 2020
# Purpose:  Originlly written to process RNA-Seq data from Amine's organoid
#           experiments. Sequencing by CeGat.

# Input parameters the script requires
PATH_FASTQ=$1 #Path to fastq files
PATH_OUT=$2 #Output path for the salmon resuts
PATH_SALMON_INDEX=$3 #Index file full path
N=$4 #Number of processors available

# All files are *.1.fastq.gz & *.2.fastq.gz
# Loop through $PATH_FASTQ and idenitfy basenames for samples based on Read 1
for fn in `find $PATH_FASTQ -name "*.1.fastq.gz" -type f`;
do
  # Extracting sample name
  samp=`basename ${fn} .1.fastq.gz`
  echo "Processing sample ${samp}"
  # Run Salmon quant
  salmon quant -i $PATH_SALMON_INDEX -l A \
            -1 ${PATH_FASTQ}/${samp}.1.fastq.gz \
            -2 ${PATH_FASTQ}/${samp}.2.fastq.gz \
            -p $N --validateMappings \
            -o ${PATH_OUT}/${samp}
done
